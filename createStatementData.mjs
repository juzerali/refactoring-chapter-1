import {plays} from "./plays.mjs";

export function createStatementData(invoice, plays) {
    const statementData = {};
    statementData.customer = invoice.customer;
    statementData.performances = invoice.performances.map(enrichPerformance);
    statementData.totalAmount = totalAmount(statementData);
    statementData.totalVolumeCredits = totalVolumeCredits(statementData);
    return statementData;
}

let playFor = function (aPerformance) {
    return plays[aPerformance.playID];
}

class PerformanceCalculator {
    constructor(aPerformance, play) {
        this.performance = aPerformance;
        this.play = play;
    }

    get amount() {
        let result = 0;
        switch (this.play.type) {
            case "tragedy":
                throw "bad-thing";
            case "comedy":
                throw "bad-thing";
            default:
                throw new Error(`unknown type: ${this.play.type}`);
        }
        return result;
    }

    get volumeCredits() {
        let result = 0;
        result += Math.max(this.performance.audience - 30, 0);

        // add extra credit for every ten comedy attendees
        if ("comedy" === this.play.type) result += Math.floor(this.performance.audience / 5);
        return result;
    }
}

class TragedyPerformanceCalculator extends PerformanceCalculator {
    get amount() {
        let result = 0;
        result = 40000;
        if (this.performance.audience > 30) {
            result += 1000 * (this.performance.audience - 30);
        }

        return result;
    }
}

class ComedyPerformanceCalculator extends PerformanceCalculator {
    get amount() {
        let result = 30000;
        if (this.performance.audience > 20) {
            result += 10000 + 500 * (this.performance.audience - 20);
        }
        result += 300 * this.performance.audience;
        return result;
    }
}

function createPerformanceCalculator(aPerformance) {
    let play = playFor(aPerformance);
    switch (play.type) {
        case "tragedy":
            return new TragedyPerformanceCalculator(aPerformance, play);
        case "comedy":
            return new ComedyPerformanceCalculator(aPerformance, play);
        default:
            throw new Error(`unknown type: ${play.type}`);
    }
}

function enrichPerformance(aPerformance) {
    const calculator = createPerformanceCalculator(aPerformance);
    const result = Object.assign({}, aPerformance);
    result.play = calculator.play;
    result.amount = calculator.amount;
    result.volumeCredits = calculator.volumeCredits;
    return result;
}

function totalVolumeCredits(data) {
    let volumeCredits = 0;
    for (let perf of data.performances) {
        // add volume credits
        volumeCredits += perf.volumeCredits;
    }
    return volumeCredits;
}

function totalAmount(data) {
    let result = 0;
    for (let perf of data.performances) {
        result += perf.amount;
    }
    return result;
}