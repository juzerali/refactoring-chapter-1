import {htmlStatement, statement} from '../statement.mjs'
import {invoices} from '../invoices.mjs'
import {plays} from '../plays.mjs'

import {expect, use} from "chai";
import chaiString from 'chai-string'
use(chaiString);

describe("statement", () => {
    it('should print the statement', function () {
        expect(statement(invoices[0], plays)).to.equalIgnoreSpaces(
            `Statement for BigCo
                Hamlet: $650.00 (55 seats)
                As You Like It: $580.00 (35 seats)
                Othello: $500.00 (40 seats)
              Amount owed is $1,730.00
              You earned 47 credits`, "statement not as expected");
    });
});

describe("htmlStatement", () => {
    it('should print the statement in HTML', function () {
        expect(htmlStatement(invoices[0], plays)).to.equalIgnoreSpaces(
            `<h1>Statement for BigCo</h1>
            <table>
                <tr><th>play</th><th>seats</th><th>cost</th></tr> <tr><td>Hamlet</td><td>55</td><td>$65,000.00</td></tr>
                <tr><td>As You Like It</td><td>35</td><td>$58,000.00</td></tr>
                <tr><td>Othello</td><td>40</td><td>$50,000.00</td></tr>
            </table> 
            <p>Amount owed is <em>$173,000.00</em></p>
            <p>You earned <em>47</em> credits</p>`, "statement not as expected");
    });
});